---
layout: layout.njk
title: About me
---

# A little bit about me

## Who am I?

I'm a Polish game programming graduate, holding a Bachelor of Science with Honours in game programming. I have completed a research project in Affective Gaming for my Honours year. I am currently living in Scotland and learning all the fun ~~swears~~slang. I advocate for open-source software, respect for privacy and reducing our digital fingerprint. I develop applications mainly in C++ and C# and have some experience with Unity, Unreal Engine and Godot. 

## My current goals

My main goal right now is to build up a portfolio and improve my current programming skills. I am currently in the planning process for an evolution style game using machine learning via genetic algorhithms. It will be a Free and Open Source project that will hopefull be going for years.

## The nearest future

I plan to complete as many courses related to my interests as possible. I believe we never stop evolving and we never stop learning and I try to expand my knowledge whenever I can. Next courses on my to-do list are the [open source CS courses by ForrestKnight](https://github.com/ForrestKnight/open-source-cs).

